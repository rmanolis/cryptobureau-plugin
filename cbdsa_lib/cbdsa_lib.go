package main

import (
	"crypto/rand"
	"encoding/gob"

	"bitbucket.org/rmanolis/cblib"
	"github.com/gopherjs/gopherjs/js"
)

func main() {
	gob.Register(cblib.Signature{})
	js.Global.Set("cbdsa", map[string]interface{}{
		"New": New,
	})

}

type Ecdsa struct {
}

func New() *js.Object {
	return js.MakeWrapper(&Ecdsa{})
}

type Key struct {
	PrivateKey string
	PublicKey  string
}

func (p *Ecdsa) GenerateKeys(curve string) (*Key, error) {
	if curve == "" {
		curve = "P256"
	}
	key, err := cblib.GenerateKeys(curve, rand.Reader)
	if err != nil {
		return nil, err
	}
	skey := Key{PublicKey: string(key.PublicKey),
		PrivateKey: string(key.PrivateKey)}
	return &skey, nil
}

func (p *Ecdsa) Verify(pub string, str string, sigb string) (bool, error) {
	return cblib.Verify([]byte(pub), []byte(str), []byte(sigb))
}

func (p *Ecdsa) VerifyFile(pub string, file []byte, sigb string) (bool, error) {
	return cblib.Verify([]byte(pub), file, []byte(sigb))
}

func (p *Ecdsa) Sign(text string, prv string) (string, error) {
	b, err := cblib.Sign([]byte(text), []byte(prv), rand.Reader)
	return string(b), err
}

func (p *Ecdsa) SignFile(file []byte, prv string) (string, error) {
	b, err := cblib.Sign(file, []byte(prv), rand.Reader)
	return string(b), err
}

func (p *Ecdsa) Encrypt(pub, text string) (string, error) {
	b, err := cblib.Encrypt([]byte(pub), []byte(text))
	return string(b), err
}

func (p *Ecdsa) EncryptFile(pub string, file []byte) ([]byte, error) {
	b, err := cblib.Encrypt([]byte(pub), file)
	return b, err
}

func (p *Ecdsa) Decrypt(priv, text string) (string, error) {
	b, err := cblib.Decrypt([]byte(priv), []byte(text))
	return string(b), err
}

func (p *Ecdsa) DecryptFile(priv string, file []byte) ([]byte, error) {
	b, err := cblib.Decrypt([]byte(priv), file)
	return b, err
}

func (p *Ecdsa) EncryptCBC(key, text string) (string, error) {
	b, err := cblib.EncryptCBC([]byte(key), text)
	return string(b), err
}

func (p *Ecdsa) DecryptCBC(key, cipher string) (string, error) {
	b, err := cblib.DecryptCBC([]byte(key), cipher)
	return string(b), err
}

func (p *Ecdsa) ComputeHmac256(key, text string) string {
	s := cblib.ComputeHmac256([]byte(key), []byte(text))
	return s
}

func (p *Ecdsa) CheckHmac256(key, text, textMac string) bool {
	return cblib.CheckHmac256([]byte(key), []byte(text), textMac)
}
