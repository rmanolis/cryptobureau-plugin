Cryptobureau is an extension for creating ECDSA keys and signatures.

This extension will help people and websites to use the power of digital signature.
There are many reasons to use digital signatures , from authenticating to voting.
But the most important reason is to minimize the bureaucracy in any organization.
This can not happen without taking the full power of what digital signature provides.
However, it is still in alpha stage without a good design.


To test it you have to follow these steps:
1) From the Keys "generate". You have to add username and password.
The password will encrypt the private key
2) After creating a key, lets "Sign". Select the key you already created.
Write something on the textbox for "Text" , add the password of your key and sign.
Copy paste the signature and text in a file.
4) Now, lets "Verify" . From the select button, select "My Keys" and select your key.
You will see your public key in a specific textbox for that.
Paste your signature and text. It very important to not forget anything from the text like the invisible characters for tabs, spaces and newlines
Press verify and if it is "verified: true" then it is verified, else you have to check that the signature or the text missing something.
5) Also you can "Encrypt" and "Decrypt" , text or files , the same way.




