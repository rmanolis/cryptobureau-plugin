import React from 'react';
import ReactDOM from 'react-dom';
import {AppCtrl}  from './AppCtrl.js';
import AppModel from './models/AppModel';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';




ReactDOM.render(<AppCtrl appModel={AppModel} />, document.getElementById('root'));