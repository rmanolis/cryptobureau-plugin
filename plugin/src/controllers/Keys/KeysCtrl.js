import React, { Component } from 'react';
import { observer } from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import Alert from 'react-s-alert';
import ReactZeroClipboard  from 'react-zeroclipboard';

@observer
class KeysCtrl extends Component {
  constructor(props) {
    super(props);
    props.keysModel.getAll();
    this.state = {
      username: "",
      email: "",
      url: "",
      curve: "",
      password: "",
      repassword: "",
      search: "",
      isLoading: false,
    }

  }

  selectKey(key) {
    console.log(key.id);
    this.props.keysModel.selectKey(key.id);
  }

  deleteKey(id) {
    console.log(id);
    this.props.keysModel.deleteKey(id);
  }

  download() {
    this.props.keysModel.download();
  }

  fileChange(event) {
    console.log('Selected file:', event.target.files[0]);
    this.props.keysModel.download();
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  handleFile = (e) => {
    const file = e.target.files[0];
    this.props.keysModel.saveFile(file);
  }

  searchUsername = (e) => {
    this.props.keysModel.search = e.target.value;
    this.props.keysModel.getAll();
  }

  listKeys() {
    const { keys, search, selectedKey } = this.props.keysModel;
    return (
      <div>
        <div>
          <input type="text" placeholder={"Search"}
            value={search} onChange={this.searchUsername} />
        </div>
        <div style={{
          "overflow": "scroll", "height": "400px",
          "width": "180px", "overflow-x": "hidden"
        }}>
          {keys.map(x => {

            let choosen = "-"
            if (selectedKey.id == x.id) {
              choosen = ">"
            }

            return (<div className="row" key={x.id} >
              <div className="col-sm-8">
                <span onClick={() => this.selectKey(x)}>
                  {choosen}  {x.username}</span>
              </div>
              <div className="col-sm-1">
                <button onClick={() => this.deleteKey(x.id)}>x</button>
              </div>
            </div>)
          }
          )}
        </div>
        <br />
        <button className="btn btn-default" onClick={() => this.download()}>Save keys</button>
        <br />
        <div>
          <label> Load contacts: </label>
          <form onSubmit={this.handleSubmit} encType="multipart/form-data">
            <input type="file" onChange={this.handleFile} accept=".json" />
          </form>
        </div>

      </div>

    )
  }

  editableInfo() {
    const key = this.props.keysModel.selectedKey;

    let editButton = (<button className="btn btn-default" onClick={() => {
      const newState = update(this.state, { isEditing: { $set: true } })
      this.setState(newState);
    }}>Edit</button>)
    let info = (<div>
      <div> <label>Username: </label>
        <span>{key.username} </span>
      </div>
      <div> <label>Email: </label>
        <span>{key.email}</span>
      </div>
      <div> <label>URL: </label>
        <span>{key.url} </span>
      </div>
    </div>)
    if (this.state.isEditing) {
      editButton = (<div>
        <button className="btn btn-default" onClick={() => {
          const newState = update(this.state, { isEditing: { $set: false } })
          this.setState(newState);
          this.props.keysModel.getAll();
          this.selectKey(key)

        }}>Close</button>
        <button className="btn btn-default" onClick={() => {
          const newState = update(this.state, { isEditing: { $set: false } })
          this.setState(newState);
          this.props.keysModel.editKey(key.id, key);
          this.props.keysModel.getAll();
          this.selectKey(key)
        }}>Submit</button>
      </div>)

      info = (<div>
        <div> <label>Username: </label>
          <input type="text"
            value={key.username}
            onChange={(event) => {
              key.username = event.target.value;
            }} />
        </div>
        <div> <label>Email: </label>
          <input type="text"
            value={key.email}
            onChange={(event) => {
              key.email = event.target.value;
            }} />
        </div>
        <div> <label>URL: </label>
          <input type="text"
            value={key.url}
            onChange={(event) => {
              key.url = event.target.value;
            }} />
        </div>
      </div>)

    }


    return (<div>
      {editButton}
      {info}
    </div>)
  }

  showSelectedKey() {
    const key = this.props.keysModel.selectedKey;
    if (key.id) {
      return (
        <div>
          {this.editableInfo()}

          <div> <label>Public key: </label>
            <div>
              <textarea id="publicKey" style={{ height: 190, width: 380 }} value={key.publicKey}></textarea>
              <button onClick={()=>{
                  let myelement = document.getElementById('publicKey')
                  myelement.select()
                  document.execCommand("copy")
              }}>Copy</button>
            </div>
          </div>
          <div> <label>Private key: </label>
            <div>
              <textarea style={{ height: 190, width: 380 }} value={key.privateKey}></textarea>
            </div>
          </div>
        </div>
      )
    } else {
      return (<div />)
    }
  }

  generateKey = () => {
    const prom = new Promise((resolve, reject) => {
      const { username, password, repassword, url,
        email, curve, seed } = this.state;
      if (!username) {
        reject("Error: Add username.");
        return;
      }
      if (password !== repassword) {
        reject("Error: Password is not correct.");
        return;
      }

      if (password.length < 4) {
        reject("Error: Password needs to be more than 4 characters.")
        return
      }

      this.props.keysModel.generateKey(username, url,
        email, curve, password, seed)
        .then(function () { resolve() })
        .catch(function (err) { reject(err) });
    });

    prom.then(() => {
      const newState = update(this.state, {
        username: { $set: "" },
        email: { $set: "" },
        url: { $set: "" },
        curve: { $set: "" },
        password: { $set: "" },
        repassword: { $set: "" },
        isLoading: { $set: false }
      })
      this.setState(newState);
    }).catch((err) => {
      const newState = update(this.state, { isLoading: { $set: false } })
      this.setState(newState);
      console.log(err);
      if (err == "Error: Malformed UTF-8 data") {
        Alert.error("Error: Password is wrong");
      } else {
        Alert.error(err);
      }
    })
  }

  render() {
    let button = <button className="btn btn-default" onClick={() => {
      const newState = update(this.state, { isLoading: { $set: true } })
      this.setState(newState);

      setTimeout(() => {
        this.generateKey()
      }, 100);
    }
    }> Generate </button>
    if (this.state.isLoading) {
      button = <span>Loading...</span>
    }

    return (
      <div className="container-fluid">
        <label> Keys </label>
        <div className="row">
          <div className="col-sm-3">
            {this.listKeys()}
          </div>
          <div className="col-sm-6">
            {this.showSelectedKey()}
          </div>
          <div className="col-sm-3">
            <h4> Generate key </h4>
            <div> <label>Username: </label>
              <input type="text" valueLink={this.linkState('username')} />
            </div>
            <br />
            <div> <label>Email: </label>
              <input type="text" valueLink={this.linkState('email')} />
            </div>
            <br />
            <div> <label>URL: </label>
              <input type="text" valueLink={this.linkState('url')} />
            </div>
            <br />
            <div> <label>Curve: </label>
              <select className="btn btn-default" valueLink={this.linkState('curve')}>
                <option value="P256">P256</option>
                <option value="P384">P384</option>
                <option value="P521">P521</option>
              </select>
            </div>
            <br />
            <div> <label> Password: </label>
              <input type="password" placeholder="Password" valueLink={this.linkState('password')} />
              <br />
              <input type="password" placeholder="Repeat password" valueLink={this.linkState('repassword')} />
            </div>
            <br />
            {button}
          </div>
        </div>
      </div>
    );
  }
}

reactMixin(KeysCtrl.prototype, LinkedStateMixin)

export default KeysCtrl;

