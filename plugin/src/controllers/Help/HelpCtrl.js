import React, {Component} from 'react';
import {observer} from 'mobx-react';

@observer
class HelpCtrl extends Component {
  constructor(props){
    super(props);
  }
  render() { 
    return (
      <div>
        <label> Help </label>
        <div>
          <div>
            <h4>Introduction</h4>
            <p> Cryptobureau is a chrome's extension for creating ECDSA keys and signatures.
              This extension will help people and websites to use the power of digital signature.
              There are many reasons to use digital signatures , from authentication to voting.
              But the most important reason is to minimize the bureaucracy in any organization.
              This can not happen without taking the full power of what digital signature provides.
              However, it is still in alpha stage without a good design.
            </p>

          </div>
          <div>
            <h4>Keys</h4>
            <p>             </p>
            This page is for generating a ECDSA keys. 
            For each key you generating needs to represent a username that you using.
            However, you can create many keys with the same username. 
            Each key you generate it needs a password. 
            With the password encrypts only the private key.
            The "curve" is about how strong you want the key. 
            But the stronger will be slower to sign or verify.
            The "256" is good enough, as it is still being used in bitcoin.
            The "Save keys" let you save all the keys in a json file.
            Also, you can load the json file you saved.
            When a key is selected it will show all the informations about the key and a button.
            The button is for submitting the username and the public key to the website.
            It is usually helpful for registration's page.
            If the website is designed to work with cryptobureau, then you will see the information of the key to the website.

          </div>
          <div>
            <h4>Sign</h4>
            <p> This page is for signing text with your selected key.
              First you select the key, then you write the text and then you can sign by adding the password you gave for that key.
            </p>

          </div>
          <div>
            <h4>Verify</h4>
            <p> 
              This page is for verifying text with signature and the public key.
            </p>

          </div>
          <div>
            <h4>Contacts</h4>
            <p> 
              This page is for saving other's people public key
            </p>

          </div>

          
        </div>
      </div>
    );
  }

};

export default HelpCtrl;


