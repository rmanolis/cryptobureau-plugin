import React, { Component } from 'react';
import { observer } from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import EcdsaSrv from '../../services/EcdsaSrv.js';
import HelperSrv from '../../services/HelperSrv.js';
import Alert from 'react-s-alert';
import ListKeysCmp from '../../components/ListKeys/ListKeysCmp.js'



@observer
class EncryptCtrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      encrypted_text: "",
      isLoading: false,
      inputType: "text",
      file: null,
    }
  }

  componentWillMount() {
    this.props.publicKeysModel.getAll();
  }

  chooseList = () => {
    const { getTypes, changeType, listType,
      getKeys, getSelectedKey, selectKey } = this.props.publicKeysModel;
    const { CONTACTS_TYPE, KEYS_TYPE } = getTypes();
    let lists = <ListKeysCmp getKeys={getKeys}
      selectKey={selectKey}
      getSelectedKey={getSelectedKey} />
    return (
      <div>
        <div>
          <select className="btn btn-default" value={listType} onChange={(e) => changeType(e.target.value)}>
            <option value={CONTACTS_TYPE}>Contacts</option>
            <option value={KEYS_TYPE}>My keys</option>
          </select>
        </div>
        <div>
          {lists}
        </div>
      </div>
    )
  }

  encrypt = () => {
    const { text, file, inputType } = this.state;
    const prom = new Promise((resolve, reject) => {
      const { selectedPublicKey } = this.props.publicKeysModel;
      if (!selectedPublicKey) {
        reject("Error: Public key is empty")
        return
      }
      if (inputType == "text") {
        if (!text) {
          reject("Error: Text is empty")
          return
        }
        EcdsaSrv.encrypt(selectedPublicKey, text).then(encrypted => {
          resolve(encrypted)
        }).catch(error => {
          reject("Error: Problem with the public key");
        })
      } else {

        if (!file) {
          reject("Error: File is empty")
          return
        }

        const reader = new FileReader();
        reader.onload = (upload) => {
          const bytes = new Uint8Array(reader.result);
          console.log(bytes)
          EcdsaSrv.encryptFile(selectedPublicKey, bytes).then(encrypted => {
            resolve(encrypted)
          }).catch(error => {
            reject("Error: Problem with the public key");
          })

        }
        reader.readAsArrayBuffer(file);

      }

    })

    prom.then((encrypted) => {
      if (inputType == "text") {
        const newState = update(this.state, {
          isLoading: { $set: false },
          encrypted_text: { $set: encrypted }
        });
        this.setState(newState);
      } else {
        const newState = update(this.state, { isLoading: { $set: false } });
        this.setState(newState);
        HelperSrv.downloadFile(encrypted, file.name + ".cbenc");
      }
    }).catch((err) => {
      const newState = update(this.state, { isLoading: { $set: false }, encrypted_text: { $set: "" } })
      this.setState(newState);
      console.log(err);
      Alert.error(err);
    })
  }

  encryptText = () => {
    const { selectedKey } = this.props.publicKeysModel;
    let username = "unknown";
    if (selectedKey && selectedKey.username) {
      username = selectedKey.username;
    }
    let button = <button className="btn btn-default" onClick={() => {
      const newState = update(this.state, { isLoading: { $set: true } })
      this.setState(newState);
      setTimeout(() => {
        this.encrypt()
      }, 100);
    }}>Encrypt</button>
    if (this.state.isLoading) {
      button = <span>Loading...</span>
    }
    return (
      <div className="row">
        <div className="col-sm-4">
          <div className="row">
            <div>
              <label>Encrypted Text: </label>
            </div>
            <textarea id="encryptedText" style={{ height: 280, width: 220 }} valueLink={this.linkState('encrypted_text')}></textarea>
            <button onClick={() => {
              let myelement = document.getElementById('encryptedText')
              myelement.select()
              document.execCommand("copy")
            }}>Copy</button>
          </div>
          <br />
          <div className="row">
            <div>
              <label>{username}'s public key: </label>
            </div>
            <textarea style={{ height: 240, width: 220 }} value={this.props.publicKeysModel.selectedPublicKey}
              onChange={e => this.props.publicKeysModel.selectedPublicKey = e.target.value}></textarea>
          </div>
        </div>
        <div className="col-sm-8">
          <div className="row">
            <div className="col-sm-5">
              {button}
            </div>
            <div className="col-sm-2">
              <select className="btn btn-default" valueLink={this.linkState('inputType')}>
                <option value="text">Text</option>
                <option value="file">File</option>
              </select>
            </div>
          </div>
          {this.selectInputType()}
        </div>
      </div>
    )
  }

  selectInputType() {
    const { inputType } = this.state;
    const textHtml = (
      <div>
        <div>
          <label>Text:</label>
        </div>
        <textarea id="text" style={{ height: 540, width: 410 }} valueLink={this.linkState('text')} />
        <button onClick={() => {
            let myelement = document.getElementById('text')
            myelement.select()
            document.execCommand("copy")
          }}>Copy</button>
      </div>
    );
    const fileHtml = (
      <div>
        <label> Upload a file: </label>
        <form onSubmit={this.handleSubmit} encType="multipart/form-data">
          <input type="file" onChange={this.handleFile} />
        </form>
      </div>
    );

    let html = textHtml;
    if (inputType == "file") {
      html = fileHtml;
    }

    return (
      <div>
        {html}
      </div>

    )
  }
  handleFile = (e) => {
    const file = e.target.files[0];
    console.log(file)
    const newState = update(this.state, { file: { $set: file } })
    this.setState(newState);
  }
  handleSubmit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <label> Encrypt </label>
        <div className="row">
          <div className="col-sm-3">
            {this.chooseList()}
          </div>
          <div className="col-sm-9">
            {this.encryptText()}
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(EncryptCtrl.prototype, LinkedStateMixin)

export default EncryptCtrl;
