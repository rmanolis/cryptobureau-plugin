import React, { Component } from 'react';
import { observer } from 'mobx-react';
import ListKeysCmp from '../../components/ListKeys/ListKeysCmp.js'
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import EcdsaSrv from '../../services/EcdsaSrv.js';
import HelperSrv from '../../services/HelperSrv.js';
import Alert from 'react-s-alert';


@observer
class DecryptCtrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      encrypted_text: "",
      decrypted_text: "",
      password: "",
      isLoading: false,
      inputType: "text",
      file: null,

    }
  }
  componentWillMount() {
    this.props.keysModel.getAll();
  }


  decryptText = () => {
    const { encrypted_text, password, inputType, file } = this.state;
    const prom = new Promise((resolve, reject) => {
      const { username } = this.props.keysModel.selectedKey
      if (!username) {
        reject("Error: Username is not selected")
        return
      } else if (!password) {
        reject("Error: Password is empty")
        return
      }
      if (inputType == "text") {
        console.log("decrypting text")
        if (!encrypted_text) {
          reject("Error: Encrypted text is empty")
          return
        }
        this.props.keysModel.decryptText(encrypted_text, password).then(decrypted => {
          resolve(decrypted)
        }).catch((err) => {
          reject(err)
        })
      } else {
        if (!file) {
          reject("Error: File is empty")
          return
        }
        if (!file.name.includes('.cbenc')) {
          reject("Error: File is not .cbenc")
          return
        }

        this.props.keysModel.decryptFile(file, password).then(decrypted => {
          resolve(decrypted)
        }).catch((err) => {
          reject(err)
        })
      }

    })
    prom.then((decrypted) => {
      if (inputType == "text") {
        const newState = update(this.state, {
          isLoading: { $set: false },
          decrypted_text: { $set: decrypted }
        })
        this.setState(newState);
      } else {
        const newState = update(this.state, {
          isLoading: { $set: false },
          file: { $set: null },
        });
        this.setState(newState);
        HelperSrv.downloadFile(decrypted, file.name.replace(".cbenc", ""));
      }
    }).catch((err) => {
      const newState = update(this.state, {
        isLoading: { $set: false },
        decrypted_text: { $set: "" },
        file: { $set: null },
      })
      this.setState(newState);
      console.log("error", err);

      if (typeof err === 'string' || err instanceof String) {
        Alert.error(err);
      } else {
        Alert.error("Error: Password is wrong");
      }
    })
  }

  selectInputType() {
    const { inputType } = this.state;
    const textHtml = (
      <div>
        <div>
          <label>Encrypted Text: </label>
        </div>
        <textarea id="encryptedText" style={{ height: 150, width: 620 }} valueLink={this.linkState('encrypted_text')} />
        <button onClick={() => {
          let myelement = document.getElementById('encryptedText')
          myelement.select()
          document.execCommand("copy")
        }}>Copy</button>
      </div>
    );
    const fileHtml = (
      <div>
        <label> Upload a file: </label>
        <form onSubmit={this.handleSubmit} encType="multipart/form-data">
          <input type="file" onChange={this.handleFile} />
        </form>
      </div>
    );

    let html = textHtml;
    if (inputType == "file") {
      html = fileHtml;
    }

    return (
      <div>
        {html}
      </div>

    )
  }

  handleFile = (e) => {
    const file = e.target.files[0];
    console.log(file)
    const newState = update(this.state, { file: { $set: file } })
    this.setState(newState);
  }
  handleSubmit(e) {
    e.preventDefault();
  }




  render() {
    let button = <button className="btn btn-default" onClick={() => {
      const newState = update(this.state, { isLoading: { $set: true } })
      this.setState(newState);
      setTimeout(() => {
        this.decryptText()
      }, 100);
    }
    }> Decrypt </button>
    if (this.state.isLoading) {
      button = <span>Loading...</span>
    }
    return (
      <div>
        <label> Decrypt </label>
        <div className="row">
          <div className="col-sm-3">
            <ListKeysCmp getKeys={() => this.props.keysModel.keys}
              selectKey={this.props.keysModel.selectKey}
              getSelectedKey={() => this.props.keysModel.selectedKey}
            />
          </div>
          <div className="col-sm-9">
            <div>
              <div>
                <label>Decrypted Text: </label>
              </div>
              <textarea id="decryptedText" style={{ height: 360, width: 620 }} value={this.state.decrypted_text}></textarea>
              <button onClick={() => {
                let myelement = document.getElementById('decryptedText')
                myelement.select()
                document.execCommand("copy")
              }}>Copy</button>
            </div>
            <div className="row">
              <div className="col-sm-9">
                <label> Password: </label>
                <input type="password" valueLink={this.linkState('password')} />
                {button}

              </div>
              <div className="col-sm-2">
                <select className="btn btn-default" valueLink={this.linkState('inputType')}>
                  <option value="text">Text</option>
                  <option value="file">File</option>
                </select>
              </div>
            </div>
            <div>
              {this.selectInputType()}
            </div>
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(DecryptCtrl.prototype, LinkedStateMixin)

export default DecryptCtrl;
