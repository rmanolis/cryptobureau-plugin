import React, { Component } from 'react';
import { observer } from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import Alert from 'react-s-alert';


@observer
class ContactsCtrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      url: "",
      email: "",
      publicKey: ""
    }
  }

  componentWillMount() {
    this.props.contactsModel.getAll();
  }

  download() {
    this.props.contactsModel.download();
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  handleFile = (e) => {
    const file = e.target.files[0];
    this.props.contactsModel.saveFile(file);
  }

  searchUsername = (e) => {
    this.props.contactsModel.search = e.target.value;
    this.props.contactsModel.getAll();
  }


  listContacts() {
    const { selectContact, deleteContact, selectedContact } = this.props.contactsModel;
    return (
      <div>
        <div>
          <input type="text" placeholder={"Search"}
            value={this.props.contactsModel.search} onChange={this.searchUsername} />
        </div>
        <div style={{
          "overflow": "scroll", "height": "400px",
          "width": "180px", "overflow-x": "hidden"
        }}>
          {this.props.contactsModel.contacts.map(x => {
            let choosen = "-"
            if (selectedContact.id == x.id) {
              choosen = ">"
            }
            return (<div className="row" key={x.id} >
              <div className="col-sm-8">
                <span onClick={() => selectContact(x.id)}> {choosen} {x.username}</span>
              </div>
              <div className="col-sm-1">
                <button onClick={() => deleteContact(x.id)}>x</button>
              </div>
            </div>)
          })}
        </div>
        <br />
        <button className="btn btn-default" onClick={() => this.download()}>Save contacts</button>
        <br />
        <div>
          <label> Load contacts: </label>
          <form onSubmit={this.handleSubmit} encType="multipart/form-data">
            <input type="file" onChange={this.handleFile} accept=".json" />
          </form>
        </div>
      </div>
    )
  }

  addContact() {
    return (
      <div>
        <h4>Add public key</h4>
        <div> <label>Username: </label>
          <div>
            <input type="text" valueLink={this.linkState('username')} />
          </div>
        </div>
        <br />
        <div> <label>Email: </label>
          <div>
            <input type="text" valueLink={this.linkState('email')} />
          </div>
        </div>
        <br />
        <div> <label>URL: </label>
          <div>
            <input type="text" valueLink={this.linkState('url')} />
          </div>
        </div>
        <div> <label>Public key: </label>
          <div>
            <textarea style={{ height: 200, width: 160 }} type="text" valueLink={this.linkState('publicKey')} />
          </div>
        </div>
        <button className="btn btn-default" onClick={() => {
          const { username, url, email, publicKey } = this.state;
          if (username) {
            this.props.contactsModel.addContact(username, url, email, publicKey);
            this.setState({
              username: "",
              url: "",
              email: "",
              publicKey: ""
            })

          } else {
            Alert.error("Error: Add username");
          }
        }}>Add contacts</button>
      </div>
    )
  }

  showSelectedContact() {
    const contact = this.props.contactsModel.selectedContact;
    if (contact.id) {
      return (
        <div>
          <div> <label>Username: </label>
            <span>{contact.username} </span>
          </div>
          <div> <label>Email: </label>
            <span>{contact.email}</span>
          </div>
          <div> <label>URL: </label>
            <span>{contact.url} </span>
          </div>
          <div> <label>Public key: </label>
            <div>
              <textarea id="contactPublicKey" style={{ height: 300, width: 380 }} value={contact.publicKey.toString()}></textarea>
              <button onClick={() => {
                let myelement = document.getElementById('contactPublicKey')
                myelement.select()
                document.execCommand("copy")
              }}>Copy</button>
            </div>
          </div>
        </div>
      )
    } else {
      return (<div />)
    }
  }



  render() {
    return (
      <div>
        <label> Contacts </label>
        <div className="row">
          <div className="col-sm-3">
            {this.listContacts()}
          </div>
          <div className="col-sm-6">
            {this.showSelectedContact()}
          </div>
          <div className="col-sm-3" >
            {this.addContact()}
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(ContactsCtrl.prototype, LinkedStateMixin)


export default ContactsCtrl;


