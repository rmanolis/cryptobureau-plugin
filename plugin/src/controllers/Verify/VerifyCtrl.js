import React, {Component} from 'react';
import {observer} from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import EcdsaSrv from '../../services/EcdsaSrv.js';
import ListKeysCmp from '../../components/ListKeys/ListKeysCmp.js'
import Alert from 'react-s-alert';

@observer
class VerifyCtrl extends Component {
  constructor(props){
    super(props); 
    this.state = {
      text:"",
      signature :"",
      isVerified:false,
      inputType: "text",
      file: null,
      isLoading:false,
    }
  }

  componentWillMount(){
    this.props.publicKeysModel.getAll();
  }

  chooseList = () =>{
    const {getTypes, changeType,listType,
      getKeys, getSelectedKey, selectKey } = this.props.publicKeysModel;
      const { CONTACTS_TYPE, KEYS_TYPE } = getTypes();
      let lists = <ListKeysCmp getKeys={getKeys} 
        selectKey={selectKey}
        getSelectedKey = {getSelectedKey}/>
        return (
          <div>
            <div>
              <select className="btn btn-default" value={listType} onChange={(e) => changeType(e.target.value)}>
                <option value={CONTACTS_TYPE}>Contacts</option>
                <option value={KEYS_TYPE}>My keys</option>
              </select>
            </div>
            <div>
              { lists }
            </div>
          </div>
        )
  }

  verify = () =>{
    const prom = new Promise((resolve,reject)=>{
      const { selectedPublicKey } = this.props.publicKeysModel;
      const { signature, text, file, inputType} = this.state;
      if(!signature){
        reject("Error: Signature is empty")
        return
      }else if(!selectedPublicKey){
        reject("Error: Public key is empty")
        return
      }
      if(inputType == "text"){
        if(!text){
          reject("Error: Text is empty")
          return
        }
        EcdsaSrv.verify(selectedPublicKey, signature, text).then(isCorrect =>{
          const newState = update(this.state,{isVerified:{$set:isCorrect}})
          this.setState(newState);
          resolve(isCorrect)
        }).catch(error=>{
            reject("Error: Problem with the signature or public key");          
        })
      }else{
        if(!file){
          reject("Error: No file exists")
          return
        }
        const reader = new FileReader();
        reader.onload = (upload) => {
          const bytes = new Uint8Array(reader.result);        
          EcdsaSrv.verifyFile(selectedPublicKey, signature, bytes).then(isCorrect =>{
            console.log("file is",isCorrect);
            const newState = update(this.state,{isVerified:{$set:isCorrect}})
            this.setState(newState);
            resolve(isCorrect)
          }).catch(error=>{
            reject("Error: Problem with the signature or public key");
          })
        }
        reader.readAsArrayBuffer(file);
      }
    })

    prom.then(()=>{
      const newState = update(this.state,{isLoading:{$set:false}})
      this.setState(newState);
    }).catch((err)=>{
      const newState = update(this.state,{isLoading:{$set:false},isVerified:{$set:false}})
      this.setState(newState);
      console.log(err);
      Alert.error(err);
    })
  }

  handleFile = (e) => {
    const {password} = this.state;
    const file = e.target.files[0];
    const newState = update(this.state,{file:{$set:file}})
    this.setState(newState);
  }
  handleSubmit(e) {
    e.preventDefault();
  }

  selectInputType =()=>{
    const { inputType} = this.state;
    const textHtml = (
      <div>
        <div>
          <label>Text: </label>
        </div>
        <textarea style={{height:540, width:420}}  valueLink={this.linkState('text')}></textarea>
      </div>
    )
    const fileHtml = (
      <div>
        <label> Upload a file: </label>
        <form onSubmit={this.handleSubmit} encType="multipart/form-data">
          <input  type="file" onChange={this.handleFile} />
        </form>
      </div>  
    )

    let html = textHtml;
    if(inputType == "file"){
      html = fileHtml;
    }

    return (
      <div>
        {html}
      </div>
    )
  }

  verifyText=() =>{
    const { selectedKey } = this.props.publicKeysModel;
    let username = "unknown";
    if(selectedKey && selectedKey.username){
      username = selectedKey.username;
    }
    let button = <button className="btn btn-default" onClick={()=>{
      const newState = update(this.state,{isLoading:{$set:true}})
      this.setState(newState);
      setTimeout(()=>{
        this.verify()
      },100);
    }}>Verify</button>
    if(this.state.isLoading){
      button = <span>Loading...</span>
    }
    return (
      <div className="row">
        <div className="col-sm-4">
          <div className="row">
            <div>
              <label>Signature: </label>
            </div>
            <textarea style={{height:200, width:210}} valueLink={this.linkState('signature')}></textarea>
          </div>
          <br/>
          <div className="row">
            <div>
              <label>{username}'s public key: </label>
            </div>
            <textarea style={{height:320, width:210}}  value={this.props.publicKeysModel.selectedPublicKey} 
              onChange={e => this.props.publicKeysModel.selectedPublicKey = e.target.value}></textarea>
          </div>

        </div>
        <div className="col-sm-8">
          <div className="row">
            <div className="col-sm-5">
              <label>Verified: </label> 
              <span> {this.state.isVerified.toString()}</span>
            </div>
            <div className="col-sm-4">
              { button }
            </div>
            <div  className="col-sm-2">
              <select className="btn btn-default" valueLink={this.linkState('inputType')}>
                <option value="text">Text</option>
                <option value="file">File</option>
              </select>
            </div>
          </div>
          <div className="row">
            { this.selectInputType() }
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div>
        <label> Verify </label>                  
        <div className="row">
          <div className="col-sm-3">
            { this.chooseList() }
          </div>
          <div className="col-sm-9">
            { this.verifyText() }
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(VerifyCtrl.prototype, LinkedStateMixin)

export default VerifyCtrl;
