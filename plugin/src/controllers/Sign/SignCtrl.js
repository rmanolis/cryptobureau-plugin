import React, { Component } from 'react';
import { observer } from 'mobx-react';
import ListKeysCmp from '../../components/ListKeys/ListKeysCmp.js'
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import Alert from 'react-s-alert';

@observer
class SignCtrl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      signature: "",
      password: "",
      inputType: "text",
      file: null,
      isLoading: false,
    }
  }
  componentWillMount() {
    this.props.keysModel.getAll();
  }


  signText = () => {
    const prom = new Promise((resolve, reject) => {
      const { username } = this.props.keysModel.selectedKey
      const { text, password, inputType, file } = this.state;
      if (!username) {
        reject("Error: Username is not selected");
        return
      } else if (!password) {
        reject("Error: Password is empty");
        return
      }
      if (inputType == "text") {
        if (!text) {
          reject("Error: Text is empty");
          return
        }
        this.props.keysModel.signText(text, password).then(signature => {
          const newState = update(this.state, { signature: { $set: signature } })
          this.setState(newState);
          resolve()
        }).catch((err) => {
          reject(err)
        })
      } else {
        if (!file) {
          reject("Error: file is empty")
          return
        }
        this.props.keysModel.signFile(file, password).then(signature => {
          const newState = update(this.state, { signature: { $set: signature } })
          this.setState(newState);
          resolve()
        }).catch((err) => {
          reject(err)
        })

      }
    })
    prom.then(() => {
      const newState = update(this.state, { isLoading: { $set: false } })
      this.setState(newState);

    }).catch((err) => {
      const newState = update(this.state, { isLoading: { $set: false } })
      this.setState(newState);
      console.log(err);

      if (typeof err === 'string' || err instanceof String) {
        Alert.error(err);

      } else {
        Alert.error('Unknown problem with signing the text');
      }
    })
  }

  handleFile = (e) => {
    const { password } = this.state;
    const file = e.target.files[0];
    const newState = update(this.state, { file: { $set: file } })
    this.setState(newState);
  }
  handleSubmit(e) {
    e.preventDefault();
  }

  selectInputType() {
    const { inputType } = this.state;
    const textHtml = (
      <div>
        <div>
          <label>Text: </label>
        </div>
        <textarea id="textToSign" style={{ height: 390, width: 630 }} valueLink={this.linkState('text')} />
        <button onClick={() => {
          let myelement = document.getElementById('textToSign')
          myelement.select()
          document.execCommand("copy")
        }}>Copy</button>
      </div>
    );
    const fileHtml = (
      <div>
        <label> Upload a file: </label>
        <form onSubmit={this.handleSubmit} encType="multipart/form-data">
          <input type="file" onChange={this.handleFile} />
        </form>
      </div>
    );

    let html = textHtml;
    if (inputType == "file") {
      html = fileHtml;
    }

    return (
      <div>
        {html}
      </div>)
  }

  render() {
    let button = <button className="btn btn-default" onClick={() => {
      const newState = update(this.state, { isLoading: { $set: true } })
      this.setState(newState);

      setTimeout(() => {
        this.signText()
      }, 100);
    }
    }> Sign </button>
    if (this.state.isLoading) {
      button = <span>Loading...</span>
    }
    return (
      <div>
        <label> Sign </label>
        <div className="row">
          <div className="col-sm-3">
            <ListKeysCmp getKeys={() => this.props.keysModel.keys}
              selectKey={this.props.keysModel.selectKey}
              getSelectedKey={() => this.props.keysModel.selectedKey}
            />
          </div>
          <div className="col-sm-9">
            <div>
              <div>
                <label>Signature: </label>
              </div>
              <textarea id="signature" style={{ height: 120, width: 630 }} value={this.state.signature}></textarea>
              <button onClick={() => {
                let myelement = document.getElementById('signature')
                myelement.select()
                document.execCommand("copy")
              }}>Copy</button>
            </div>
            <div className="row">
              <div className="col-sm-10">
                <label> Password: </label>
                <input type="password" valueLink={this.linkState('password')} />
                {button}
              </div>
              <div className="col-sm-2">
                <select className="btn btn-default" valueLink={this.linkState('inputType')}>
                  <option value="text">Text</option>
                  <option value="file">File</option>
                </select>
              </div>

            </div>
            {this.selectInputType()}
          </div>
        </div>
      </div>
    );
  }

};

reactMixin(SignCtrl.prototype, LinkedStateMixin)

export default SignCtrl;
