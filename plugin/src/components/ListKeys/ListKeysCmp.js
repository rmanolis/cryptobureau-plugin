import React, {Component} from 'react';
import {observer} from 'mobx-react';
import reactMixin from 'react-mixin'
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import update from 'react-addons-update';
import _ from 'lodash';

//props.keys { id, name, url }
//props.selectKey(id)
@observer
class ListKeysCmp extends Component {
  constructor(props){
    super(props); 
    this.state = {
      search : ""
    }
  }

  propTypes: {
    selectKey: React.PropTypes.func,
    getKeys:      React.PropTypes.func,
    getSelectedKey: React.PropTypes.func,
  }

  componentDidMount(){
    const keys = this.props.getKeys();
    if(keys && keys.length > 0 ){
      this.props.selectKey(keys[0].id)
    }
  }


  listNames(){
    const { getKeys, selectKey, getSelectedKey } = this.props;
    const keys = _.filter(getKeys(), key => {
      return key.username.startsWith(this.state.search);       
    });
    return (
      <div style={{"overflow": "scroll","height":"500px",
          "width":"180px","overflow-x": "hidden"}}>
          { keys.map(x =>{ 
                   let choosen = "-"
                   if(getSelectedKey().id == x.id){
                    choosen = ">"
                   }
                   return (<div className="row" key={x.id} >
                     <div className="col-sm-8">
                       <span onClick={() => selectKey(x.id)}>{choosen} { x.username }</span>
                     </div>
                   </div>)
                   }
                   ) }
                 </div>
    )
  }

  searchName(){
    return (
      <div>
        <input type="text" placeholder={"Search"} valueLink={this.linkState('search')} />        
      </div>
    )
  }

  selected(){
    const selectedKey = this.props.getSelectedKey();
    let name 
    if(selectedKey && selectedKey.username){ 
      name = selectedKey.username
    } else{
      name = "nothing"
    }

    return (
      <div>
        <label>selected: </label>        
        <span>{name}</span>    
      </div>
    )
  }

  render() {
    return (
      <div>
        <div>
          {this.selected()}
        </div>
        <div>
          {this.searchName()}
        </div>
        <div>
          {this.listNames()}
        </div>
      </div>
    )
  }
}

reactMixin(ListKeysCmp.prototype, LinkedStateMixin)

export default ListKeysCmp;

