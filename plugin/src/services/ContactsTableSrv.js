import {  DB } from './DBCreatorSrv.js';

function transGet(id,data){
  return { 
    id, 
    publicKey : data.publicKey,
    username: data.username,
    email: data.email,
    url: data.url,
  }
}

function transSet(data){
  return { 
    publicKey : data.publicKey,
    username: data.username,
    email: data.email,
    url: data.url,
  }
}

const obj = new DB("contacts", transGet, transSet ); 

export default obj
