import { Key } from './DBCreatorSrv.js';

const ecdsa = window.cbdsa.New();


const obj = {
  generateKey : function(curve){
    return new Promise(function(resolve, reject){
      const [key, error] = ecdsa.GenerateKeys(curve)
      if(error){
        reject(error);
      }else{
        resolve({publicKey: key.PublicKey, 
                privateKey: key.PrivateKey});
      }
    });
  },
  sign: function(privateKey, text){
    return new Promise(function(resolve, reject){
      
      const [signature, error] = ecdsa.Sign(text, privateKey)
      if(error){
        reject(error);
      }else{
        resolve(signature);
      }
    });
  },
  signFile: function(privateKey, file){
    return new Promise(function(resolve, reject){
      
      const [signature, error] = ecdsa.SignFile(file, privateKey)
      if(error){
        reject(error);
      }else{
        resolve(signature);
      }
    });
  },
  verify: function(publicKey, signature, text ){
     return new Promise(function(resolve, reject){
        const [isCorrect, error] = ecdsa.Verify(publicKey,text, signature)
        if(error){
          reject(error);
        }else{
          resolve(isCorrect);
        }
      });
  },
  verifyFile: function(publicKey, signature, file ){
     return new Promise(function(resolve, reject){
        const [isCorrect, error] = ecdsa.VerifyFile(publicKey,file, signature)
        if(error){
          reject(error);
        }else{
          resolve(isCorrect);
        }
      });
  },
  encrypt: function(publicKey, text){
     return new Promise(function(resolve, reject){
        const [enc_text, error] = ecdsa.Encrypt(publicKey,text)
        if(error){
          reject(error);
        }else{
          resolve(enc_text);
        }
      });
  },
  encryptFile: function(publicKey, file){
     return new Promise(function(resolve, reject){
        const [enc, error] = ecdsa.EncryptFile(publicKey,file)
        if(error){
          reject(error);
        }else{
          resolve(enc);
        }
      });
  },

  decrypt: function(privateKey, text){
    return new Promise(function(resolve, reject){
        const [dec_text, error] = ecdsa.Decrypt(privateKey,text)
        if(error){
          reject(error);
        }else{
          resolve(dec_text);
        }
      });
  },
  decryptFile: function(privateKey, file){
    return new Promise(function(resolve, reject){
        const [dec, error] = ecdsa.DecryptFile(privateKey,file)
        if(error){
          reject(error);
        }else{
          resolve(dec);
        }
      });
  }


}


export default obj;
