import uuid from 'node-uuid';
import _ from 'lodash';

export class DB{
  constructor(table, transGet, transSet){
    this.table = table;
    this.transGet = transGet;
    this.transSet = transSet;
  }

  getAll(){
    const {table, transGet} = this;        
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          if(!data[table]){
            reject("no data")
            return
          }
          let keys = [];
          _.forOwn(data[table], function(value, key) { 
            keys.push(transGet(key,value))
          } );
          resolve(keys);
        }else{
          reject(chrome.runtime.error)
        }
      });
    })
  }

  add(values){
    const {table, transSet} = this;
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          let keys = data[table];
          if(!keys){
            keys = {};
          }
          const id = uuid.v4();
          keys[id] = transSet(values);
          data[table] = keys;
          chrome.storage.local.set(data, function() {
            if (chrome.runtime.error) {
              reject(chrome.runtime.error);
            }else{
              resolve(id);
            }
          });
        }else{
          reject(chrome.runtime.error)
        }
      });
    })
  }


  bulk(values){
    const {table, transSet} = this;
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          let keys = data[table];
          if(!keys){
            keys = {};
          }
          for (var value of values) {
            const id = uuid.v4();
            keys[id] = transSet(value);
          }
          data[table] = keys;
          chrome.storage.local.set(data, function() {
            if (chrome.runtime.error) {
              reject(chrome.runtime.error);
            }else{
              resolve();
            }
          });
        }else{
          reject(chrome.runtime.error)
        }
      });
    })


  }

  searchValue(attr, searchValue){
    const {table, transGet} = this;    
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {

        if (!chrome.runtime.error) {
          if(!data[table]){
            reject(`search error for ${table}: no data`)
            return
          }
          const keys = data[table];
          _.forEach(keys, function(key, id) {
            _.forEach(key, function(v,k){
              
              if(attr === k){
               if( v.trim() === searchValue.trim()){
                 resolve(transGet(id, key))
               }
              }
            })
          });
          reject(`search error for ${table}: no value found`);
          
        }else{
          reject(chrome.runtime.error)
        }
      });

    });
  }

  get(id){
    const {table, transGet} = this;    
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {

        if (!chrome.runtime.error) {
          if(!data[table]){
            reject("no data")
            return
          }
          const keys = data[table];
          if(keys[id]){
            const value = keys[id];
            resolve(transGet(id, value))
          }else{
            reject("no data");
          }
        }else{
          reject(chrome.runtime.error)
        }
      });

    });
  }

  delete(id){
    const {table} = this;  
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          if(!data[table]){
            reject("no data")
            return
          }

          let keys = data[table];
          if(keys[id]){
            delete keys[id];
            const newKeys = _.omit(keys,id);
            data[table] = newKeys;
            chrome.storage.local.set(data, function() {
              if (chrome.runtime.error) {
                reject(chrome.runtime.error);
              }else{
                resolve();
              }
            });
          }else{
            reject("no data");
          }
        }else{
          reject(chrome.runtime.error)
        }
      });

    });
  }

  edit(id, key){
    const {table} = this;  
    return new Promise(function(resolve, reject){
      chrome.storage.local.get(table, function(data) {
        if (!chrome.runtime.error) {
          if(!data[table]){
            reject("no data")
            return
          }

          let keys = data[table];
          if(keys[id]){
            keys[id] = key;
            const newKeys = keys;
            data[table] = newKeys;
            chrome.storage.local.set(data, function() {
              if (chrome.runtime.error) {
                reject(chrome.runtime.error);
              }else{
                resolve();
              }
            });
          }else{
            reject("no data");
          }
        }else{
          reject(chrome.runtime.error)
        }
      });

    });
  }

}
