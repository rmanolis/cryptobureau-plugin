import {  DB } from './DBCreatorSrv.js';



function transGet(id,data){
  return { 
    id, 
    privateKey: data.privateKey,
    publicKey : data.publicKey,
    hmacPrivateKey: data.hmacPrivateKey,
    hmacPublicKey : data.hmacPublicKey,
    username: data.username,
    email: data.email,
    url: data.url,
    curve: data.curve,
  }
}

function transSet(data){
  console.log("set key", data);
  return { 
    privateKey: data.privateKey,
    publicKey : data.publicKey,
    hmacPrivateKey: data.hmacPrivateKey,
    hmacPublicKey : data.hmacPublicKey,
    username: data.username,
    email: data.email,
    url: data.url,
    curve: data.curve,
  }
}

const obj = new DB("keys", transGet, transSet ); 

export default obj
