const ecdsa = window.cbdsa.New();


const CryptoSrv = new class CryptoJsSrv {

  encrypt(str, password){
    const res = ecdsa.EncryptCBC(password, str);
    console.log(res);
    return res
  }

  decrypt(ciphertext, password){
    return ecdsa.DecryptCBC(password, ciphertext)
  }
  
  computeHmac(str, password){
   const hmac = ecdsa.ComputeHmac256(password, str);
   return hmac
  }

  checkHmac(str,strHmac , password){
   const b = ecdsa.CheckHmac256(password, str, strHmac);
   return b
  }

  
}

export default CryptoSrv


