import {observable, computed} from 'mobx';
import EcdsaSrv from '../services/EcdsaSrv.js';
import ContactsTableSrv from '../services/ContactsTableSrv.js';
import HelperSrv from '../services/HelperSrv.js';

const ContactsModel =  new class ContactStore {
  @observable contacts = [];
  @observable selectedContact = {};
  @observable search = "";
  


  getAll(){
    ContactsTableSrv.getAll().then(function(data){
      this.contacts = _.filter(data, key => {
        return key.username.startsWith(this.search);       
      });
      if(data.length > 0){
        this.selectedContact = data[0]
      }


    }.bind(this))
  }

  getId(id){
    return ContactsTableSrv.get(id)
  }

  searchPublicKey = (publicKey) =>{
    return ContactsTableSrv.searchValue("publicKey",publicKey)
  }
  
  selectContact = (id)=>{
    ContactsTableSrv.get(id).then(data=>{
      this.selectedContact = data;
    })
  }

  download(){
    HelperSrv.download(this.contacts,"contacts.json")
  }

  saveFile(file){
    const self = this;    
    const reader = new FileReader();
    reader.onload = function(upload) {
      const json = JSON.parse(reader.result);
      ContactsTableSrv.bulk(json).then(function(){
       self.getAll();
      })
    }
    reader.readAsText(file);
  }


  deleteContact =(id) =>{
    ContactsTableSrv.delete(id).then(() => {
      this.getAll();
      if(this.contacts.length == 0){
        this.selectedContact = {};
      }
      if( this.selectedContact && id == this.selectedContact.id){
        this.selectedContact = {};
      }
    }).catch(function(error){
      console.log(error)
    })
  }

  addContact(username, url, email, publicKey){
    const contact = {
      username,
      url,
      email,
      publicKey,
    }
    ContactsTableSrv.add(contact).then(id => {
      console.log(id);
      ContactsTableSrv.get(id).then(data => {
        console.log(data)
        this.contacts.push(data);
      })
    })
  }

}();

export default ContactsModel;
