import {observable, computed, observe} from 'mobx';
import KeysTableSrv from '../services/KeysTableSrv.js';
import ContactsTableSrv from '../services/ContactsTableSrv.js';


const CONTACTS_TYPE = "contacts";
const KEYS_TYPE = "keys";

const PublicKeysModel =  new class PublicKeysStore {


  @observable verifiableKeys = [];
  @observable selectedKey = {};
  @observable selectedPublicKey = "";
  @observable listType = CONTACTS_TYPE;



  getTypes(){
    return { CONTACTS_TYPE, KEYS_TYPE };
  }
  getAll = () =>{
    console.log("getting by type")
    if(this.listType == CONTACTS_TYPE){
      ContactsTableSrv.getAll().then((data) => {
        this.verifiableKeys = data;
      })
    }else if(this.listType == KEYS_TYPE){
      KeysTableSrv.getAll().then((data) => {
        this.verifiableKeys = data;
      })
    }
    console.log(this.verifiableKeys)    
  }

  getSelectedKey = () => {
    return this.selectedKey
  }

  changeType = (str) =>{
    console.log("changing type to", str);
    this.listType = str;
    this.getAll();
  }

  selectKey = (id) => {
    console.log("selected key", id);
    if(this.listType == CONTACTS_TYPE){
      ContactsTableSrv.get(id).then(key =>{
        this.selectedKey = key
        this.selectedPublicKey = key.publicKey;
      })
    }else if(this.listType == KEYS_TYPE){
      KeysTableSrv.get(id).then(key =>{
        this.selectedKey = key   
        this.selectedPublicKey = key.publicKey;        
      })
    }
  }

  getKeys = () => {
    return this.verifiableKeys
  }

  findPublicKey = (publicKey) =>{ 
    console.log("find public key", publicKey)
    return new Promise((resolve, reject) => {
      ContactsTableSrv.searchValue("publicKey",publicKey).then(function(data){
        console.log("found it in contacts")
        this.listType = CONTACTS_TYPE;
        this.selectedKey = data     
        this.selectedPublicKey = data.publicKey;     
        this.getAll();
        resolve(true)
      }).catch(() => {
        KeysTableSrv.searchValue("publicKey",publicKey).then((data)=>{
          console.log("found it in your keys")
          this.listType = KEYS_TYPE;
          this.selectedKey = data         
          this.selectedPublicKey = data.publicKey;
          this.getAll();
          resolve(true)
        }).catch(function(e){
          console.log(e)
          resolve(false)
        })
      })
    })
  }

}
export default PublicKeysModel;
