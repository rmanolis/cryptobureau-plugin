import {observable, computed} from 'mobx';


const AppModel =  new class AppStore {
  @observable page = "/";

  changePage(page){
    this.page = page;
  }
}();

export default AppModel;
