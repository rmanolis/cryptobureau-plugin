import {observable, computed} from 'mobx';
import EcdsaSrv from '../services/EcdsaSrv.js';
import KeysTableSrv from '../services/KeysTableSrv.js';
import CryptoSrv from '../services/CryptoSrv.js';
import HelperSrv from '../services/HelperSrv.js';
import _ from 'lodash';

const KeysModel = new class KeysStore {
  @observable keys = [];
  @observable selectedKey = {};
  @observable search = "";

  getAll() {
    KeysTableSrv.getAll().then(function (data) {
      this.keys = _.filter(data, key => {
        return key.username.startsWith(this.search);
      });
      if (data.length > 0) {
        this.selectedKey = data[0]
      }
    }.bind(this))
  }

  getId(id) {
    return KeysTableSrv.get(id)
  }

  searchPublicKey = (publicKey) => {
    return KeysTableSrv.searchValue("publicKey", publicKey)
  }

  searchUsername = (username) => {
    return KeysTableSrv.searchValue("username", username)
  }


  download() {
    HelperSrv.downloadJson(this.keys, "keys.json")
  }


  selectKey = (id) => {
    KeysTableSrv.get(id).then(data => {
      this.selectedKey = data;
    })
  }

  signText(text, password) {
    const [pk, _] = CryptoSrv.decrypt(this.selectedKey.privateKey, password);
    return EcdsaSrv.sign(pk, text)
  }

  decryptText(encrypted_text, password) {
    const [pk, _] = CryptoSrv.decrypt(this.selectedKey.privateKey, password);
    return EcdsaSrv.decrypt(pk, encrypted_text)
  }

  signFile(file, password) {
    const self = this;
    return new Promise(function (resolve, reject) {
      const reader = new FileReader();
      reader.onload = function (upload) {
        const bytes = new Uint8Array(reader.result);
        try {
          const pk = CryptoSrv.decrypt(self.selectedKey.privateKey, password);
          resolve(EcdsaSrv.signFile(pk, bytes))
        } catch (e) {
          reject(e)
        }
      }
      reader.readAsArrayBuffer(file);
    })
  }

  decryptFile(file, password) {
    const self = this;
    return new Promise(function (resolve, reject) {
      const reader = new FileReader();
      reader.onload = (upload) => {
        const bytes = new Uint8Array(reader.result);
        try {
          const pk = CryptoSrv.decrypt(self.selectedKey.privateKey, password);
          resolve(EcdsaSrv.decryptFile(pk, bytes))
        } catch (e) {
          reject(e)
        }
      }
      reader.readAsArrayBuffer(file);
    })
  }

  generateKey(username, url, email, curve, password) {
    const self = this;
    return new Promise((resolve, reject) => {
      EcdsaSrv.generateKey(curve).then(function (gkey) {
        let key = {};
        key.username = username;
        key.url = url;
        key.email = email;
        key.curve = curve;
        key.publicKey = gkey.publicKey
        key.hmacPublicKey = CryptoSrv.computeHmac(gkey.publicKey, password);
        const [enc_pk, err] = CryptoSrv.encrypt(gkey.privateKey, password);
        if(err){
          reject(err);
          return
        }
        key.privateKey = enc_pk
        key.hmacPrivateKey = CryptoSrv.computeHmac(key.privateKey, password);
        KeysTableSrv.add(key).then(function (id) {
          KeysTableSrv.get(id).then(function (data) {
            self.keys.push(data);
            self.selectedKey = data;
          }.bind(this))
        })
        resolve()
      }).catch(function (err) {
        reject(err)
      })
    })
  }

  saveFile(file) {
    const self = this;
    const reader = new FileReader();
    reader.onload = function (upload) {
      const json = JSON.parse(reader.result);
      KeysTableSrv.bulk().then(function () {
        self.getAll();
      })
    }
    reader.readAsText(file);
  }

  deleteKey(id) {
    KeysTableSrv.delete(id).then(data => {
      this.getAll();
      if (this.keys.length == 0) {
        this.selectedKey = {};
      }
      if (this.selectedKey && id == this.selectedKey.id) {
        this.selectedKey = {};
      }
    }).catch(function (error) {
      console.log(error)
    })
  }

  editKey(id, key){
    KeysTableSrv.edit(id,key).then(data => {
      this.getAll();
      KeysTableSrv.get(id).then(data => {
        this.selectedKey = data;
      })

    }).catch(function (error) {
      console.log(error)
    })
  }

}();

export default KeysModel;
