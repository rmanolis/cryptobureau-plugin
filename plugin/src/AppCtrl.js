import React from 'react';
import {Router} from 'director';
import {when} from "mobx";
import {observer} from "mobx-react";
import Alert from 'react-s-alert';
//controllers
import KeysCtrl from './controllers/Keys/KeysCtrl';
import ContactsCtrl from './controllers/Contacts/ContactsCtrl';
import SignCtrl from './controllers/Sign/SignCtrl';
import VerifyCtrl from './controllers/Verify/VerifyCtrl';
import DecryptCtrl from './controllers/Decrypt/DecryptCtrl';
import EncryptCtrl from './controllers/Encrypt/EncryptCtrl';
import HelpCtrl from './controllers/Help/HelpCtrl';

//models
import KeysModel from './models/KeysModel.js';
import ContactsModel from './models/ContactsModel.js';
import PublicKeysModel from './models/PublicKeysModel.js';

@observer
class NavMenu extends React.Component{
  pages = [
    { url:'/keys', title:'Keys'}, 
    { url:'/contacts', title:'Contacts'},      
    { url:'/sign', title:'Sign'},
    { url:'/verify', title:'Verify'},
    { url:'/decrypt', title:'Decrypt'},
    { url:'/encrypt', title:'Encrypt'},
    { url:'/', title:'Help'},    
  ];

  render(){
    return (
      <div className="collapse navbar-collapse">
        <ul className="nav navbar-nav">
        { 
        this.pages.map(page => {
          return <li  key={page.url} ><a  onClick={()=> this.props.appModel.changePage(page.url)} > {page.title} </a></li>
          })
        }
        </ul>
      </div>
    )
  }
}


@observer
export class AppCtrl extends React.Component {

  constructor(props) {
    super(props);
  }

  getPage = () => {
    switch (this.props.appModel.page) {
      case "/":
        return <HelpCtrl />
      case "/keys":
        return <KeysCtrl keysModel={KeysModel} />
      case "/sign":
        return <SignCtrl keysModel={KeysModel}/>
      case "/contacts":
        return <ContactsCtrl contactsModel={ContactsModel}  />
      case "/verify":
        return <VerifyCtrl publicKeysModel={PublicKeysModel}   />
      case "/decrypt":
        return <DecryptCtrl keysModel={KeysModel} />
      case "/encrypt":
        return <EncryptCtrl publicKeysModel={PublicKeysModel}   />
    }
  }

  render() {
    const Children = this.props.appModel.component;
    return (
      <div className="container-fluid">
        <div className="appContent">
          <div>
            <NavMenu appModel={this.props.appModel}/>
          </div>
          { 
          this.getPage()
          }
        </div>
        <Alert stack={{limit: 3}} />
      </div>
    );
}

}


