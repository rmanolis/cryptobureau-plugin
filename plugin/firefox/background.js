console.log("Cryptobureau injected");


var onChange = new Event('change');

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  console.log(request.action);
  if (request.action === "getAction"){
    var elem = document.getElementById('cb-action');
    if(elem){
      sendResponse({data: elem.innerText});
    }else{
      console.log("could not find cb-action");      
    }
  }else if(request.action === "getText"){
    var elem = document.getElementById('cb-text');
    if(elem){
      sendResponse({data: elem.innerText});
    }else{
      console.log("could not find cb-text");      
    }
  }else if(request.action === "getSignature"){
    var elem = document.getElementById('cb-signature');
    if(elem){
      sendResponse({data: elem.innerText});
    }else{
      console.log("could not find cb-signature");      
    }

  }else if(request.action === "getPublicKey"){
    var elem = document.getElementById('cb-public-key');
    if(elem){
      sendResponse({data: elem.innerText});
    }else{
      console.log("could not find cb-public-key");      
    }

  }else if(request.action === "getUsername"){
    var elem = document.getElementById('cb-username');
    if(elem){
      sendResponse({data: elem.innerText});
    }else{
      console.log("could not find cb-username");      
    }

  }else if(request.action === "getEncryptedText"){
    var elem = document.getElementById('cb-encrypted-text');
    if(elem){
      sendResponse({data: elem.innerText});
    }else{
      console.log("could not find cb-encrypted-text");      
    }

  }else if(request.action === "setSignature") {
    var elem = document.getElementById('cb-set-signature');
    if(elem){
      elem.value = request.data;
      elem.dispatchEvent(onChange);
    }else{
      console.log("could not find cb-set-signature");
    }
  }else if(request.action === "setPublicKey") {
    var elem = document.getElementById('cb-set-public-key');
    if(elem){
      elem.value = request.data;
      elem.dispatchEvent(onChange);      
    }else{
      console.log("could not find cb-set-public-key");
    }

  }else if(request.action === "setUsername"){
    var elem = document.getElementById('cb-set-username');
    if(elem){
     elem.value = request.data;
     elem.dispatchEvent(onChange);
    }else{
      console.log("could not find cb-set-username");
    }
   }else if(request.action === "setEncryptedText"){
    var elem = document.getElementById('cb-set-encrypted-text');
    if(elem){
     elem.value = request.data;
     elem.dispatchEvent(onChange);
    }else{
      console.log("could not find cb-set-encrypted-text");
    }
  }else if(request.action === "setDecryptedText"){
    var elem = document.getElementById('cb-set-decrypted-text');
    if(elem){
     elem.value = request.data;
     elem.dispatchEvent(onChange);
    }else{
      console.log("could not find cb-set-decrypted-text");
    }
  }else{
    sendResponse({}); // Send nothing..
  }
});
