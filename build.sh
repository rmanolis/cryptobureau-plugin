#!/bin/bash
go get bitbucket.org/rmanolis/cblib
gopherjs build cbdsa_lib/cbdsa_lib.go 
cp cbdsa_lib.js plugin/chrome/libs/cbdsa_lib.js
cp cbdsa_lib.js plugin/extension-dev/libs/cbdsa_lib.js
cp cbdsa_lib.js plugin/firefox/libs/cbdsa_lib.js
cd plugin

npm run build-firefox
npm run build-chrome

cd ..

rm cbdsa_lib.js  cbdsa_lib.js.map 